$(document).ready(function () {
    $('select').material_select();
});

document.querySelector('#regbutton').addEventListener("click", (e) => {
    e.preventDefault();
    toastr.remove();
    let error = false;
    const inscriere = {
        nume: document.querySelector('#nume').value,
        prenume: document.querySelector('#prenume').value,
        telefon: document.querySelector('#telefon').value,
        mail: document.querySelector('#mail').value,
        universitate: document.querySelector('#universitate').value,
        facultate: document.querySelector('#facultate').value,
        domeniul: document.querySelector('#domeniul').value,
        activitate1: document.querySelector("#activitate1").value,
        activitate2: document.querySelector("#activitate2").value,
        activitate3: document.querySelector("#activitate3").value,
        an_studiu: document.querySelector("#an_studiu").value,
        GDPR: document.querySelector("#GDPR").value
    }
    console.log(GDPR);
    if ((parseInt(inscriere.an_studiu)) === NaN) {
        error = true;
        toastr.error("Campul permite doar cifre!");
    } else if ((parseInt(inscriere.telefon)) === "NaN") {
        error = true;
        toastr.error("Campul permite doar cifre!");
    } 

if(!error){
    axios.post('/inscriere', inscriere)
        .then((response) => {
            toastr.success("Inscriere adaugata!");
        })
        .catch((error) => {
            const values = Object.values(error.response.data)
            console.log(error);
            values.map(item => {
                toastr.error(item)
            })
        })
    }
}, false)